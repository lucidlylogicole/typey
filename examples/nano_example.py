import sys, os
sys.path.append('../')
import typey

prompt = typey.Prompter()

@prompt.command('note',help='write a note in nano')
def temp_note():
    # Create Temporary File
    import tempfile
    f = tempfile.NamedTemporaryFile(mode='w+t', delete=False)
    n = f.name
    f.close()
    
    # Open Nano with temporary file
    os.system("nano "+n)
    
    sys.stdout.write('') # helps realign the cursor
    
    # Get the temporary file text
    with open(n) as f:
        txt = f.read()
    print(txt)
        
    # Delete  the temporary file
    os.remove(n)

# Start App
print('1. Type \'note\' to start a note in the Nano editor')
print('2. Write your note in Nano')
print('3. Save and Exit Nano')
prompt.start()