import sys
sys.path.append('../')
import typey

prompt = typey.Prompter('ex')

# Main Commands
@prompt.command('fib', help='liar')
def fibber():
    print('the sky is fake')

# Calculator App
calc_app = typey.App(prompt,'calc')

@calc_app.command('fib', help='fibonacci calculator')
def fibonacci(n=1):
    n = int(n)
    if n == 0:
        print(0)
    elif n < 0:
        print('Error: negative number cannot be used')
    else:
        t = 1
        for i in range(1,n+1):
            t = t*i
        print(t)

# Start App
if len(sys.argv) > 1:
    prompt.start(' '.join(sys.argv[1:]))
else:
    prompt.start()
