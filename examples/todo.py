import sys
sys.path.append('../')
import typey

prompt = typey.Prompter()

todo_list = ['Add an item to this list','Delete an item from this list']

@prompt.command('add', help='add todo item')
def add_item(item=None):
    if item == None:
        print('  ERROR: enter text after the add command to add an item')
    else:
        todo_list.append(item)
        list_items()

@prompt.command('list', help='list todo items')
def list_items():
    prompt.clear()
    print('-------TODO LIST--------')
    i = 0
    for item in todo_list:
        i += 1
        print(i,' ',item)
    print()
    print('commands: add, del, exit')
    print()

@prompt.command('del', help='delete a todo item')
def del_items(item_no=None):
    try:
        i = int(item_no)
    except:
        print('  ERROR: enter an integer corresponding to the item to delete')
        return
    
    if i > 0 and i < len(todo_list)+1:
        del todo_list[i-1]
        list_items()
    else:
        print('  ERROR: the value is out of range for the item')


# Start App
prompt.start('list')
