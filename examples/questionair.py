import sys
sys.path.append('../')
import typey

prompt = typey.Prompter()

name = prompt.input('Name:')
age = prompt.input('Age:')
dsc = prompt.minput('Description:')

print('---------------------------')
print('| '+prompt.color(typey.colors.orange,'Name'+': ')+name)
print('| '+prompt.color(typey.colors.orange,'Age'+': ')+age)
print('| '+prompt.color(typey.colors.orange,'Description'+': '))
for ln in dsc.splitlines():
    print('|   '+ln)