import sys
sys.path.append('../')
import typey
prompt = typey.Prompter()

@prompt.command('fib', help='fibonacci calculator')
def fibonacci(n=1):
    if n == 0:
        print(0)
    else:
        t = 1
        for i in range(1,int(n)+1):
            t = t*i
        print(t)

@prompt.command('clear', help='clear')
def clear(n=1):
    prompt.clear()
    print('What?')

if len(sys.argv) > 1:
    prompt.start(' '.join(sys.argv[1:]))
else:
    prompt.start()

