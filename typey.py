import sys,os,traceback

version='1.2.0'

class Prompter:
    def __init__(self, name=''):
        self.commands = {}
        self.apps = {}
        self.show_errors = True
        self.show_not_found = True
        self.name = name
        self.mode = ''
        self.current_app = None
        if os.name == 'nt':
            self.prompt_color = None
            self.error_color = None
        else:
            self.prompt_color = colors.lightblue
            self.error_color = colors.lightred
    
    def getPromptString(self):
        appnm = self.name
        if self.current_app != None:
            appnm = self.current_app
        if appnm != '' and self.mode != '':
            appnm +=':'
        return self.color(appnm+self.mode+'> ',self.prompt_color)
    
    def prompt(self,text=None):
        '''Prompt and parse the input'''
        if text == None:
            text = input(self.getPromptString())
        self.runInput(text)
        self.prompt()
        
    def runInput(self,text):
        '''Parse command and run function'''
        cmds = text.split(' ')
        cmd = cmds[0]

        # Check for exit
        if text == 'clear':
            self.clear()
        elif self.current_app != None and text == 'exit':
            self.current_app = None
        elif text == 'exit' or text == 'exit!':
            self.current_app = None
            self.exit()
        elif text == 'help':
            self.help()
        else:
            funcD = None
            found = 0
            if self.current_app != None:
                # Run App Command
                if cmd in self.apps[self.current_app]['commands']:
                    funcD = self.apps[self.current_app]['commands'][cmd]
                    found = 1
            else:
                # Run if main command
                if cmd in self.commands:
                    funcD = self.commands[cmd]
                    found = 1
                elif cmd in self.apps:
                    # Start App if app
                    self.openApp(cmd)
                    found = 1



            # Run function if found
            if funcD:
                try:
                    if len(cmds) > 1:
                        if cmds[1] == '-h':
                            # Print Help
                            print(funcD['h'])
                        else:
                            # Run Command
                            funcD['f'](' '.join(cmds[1:]))
                    else:
                        funcD['f']()
                except Exception:
                    if self.show_errors:
                        if self.error_color != None:
                            print("\033["+self.error_color+"m\033[A")
                        traceback.print_exc()
                        if self.error_color != None:
                            print("\033[00m")
                    else:
                        print(self.color('   ERROR',self.error_color))
            elif self.show_not_found and not found:
                print('  command not found')
    
    def start(self, command=None):
        '''start prompter'''
        self.prompt(text=command)
    
    def exit(self):
        '''exit app'''
        sys.exit()

    def input(self,text=''):
        '''Get Input with formatted prompt'''
        return input(self.color(text+' ',self.prompt_color))
    
    def minput(self,text=''):
        '''Get multiple lines of input with a blank line triggering the end'''
        t = ''
        if text: print(self.color(text,self.prompt_color))
        inp = input()
        while inp != '':
            if t != '': t += '\n'
            t += inp
            inp = input()
        return t
    
    def color(self,text,color_code=None):
        '''Generate ANSI terminal color around text'''
        if color_code == None:
            return text
        return "\033[{0}m{1}\033[00m".format(color_code,text)

    def clear(self):
        '''clear the terminal'''
        print("\033c", end="")
    
    def help(self):
        '''display help'''
        print()
        if self.current_app == None:
            help_commands = self.commands
            nm = self.name.upper()
            if nm != '': nm += ' '
            print('--- {0}HELP ---'.format(nm))
        else:
            help_commands = self.apps[self.current_app]['commands']
            print('--- {0} HELP ---'.format(self.current_app.upper()))
            print()
        
        if help_commands != {}:
            
            # Get Max Command Length
            mx = 0
            for key in help_commands:
                lkey = len(key)
                if lkey > mx: mx = lkey
            
            print('COMMANDS')
            for key in help_commands:
                print('  '+key.ljust(mx+2,' '),help_commands[key]['h'])
            print()
        
        if self.current_app == None and self.apps != {}:
            mx = 0
            for key in self.apps:
                lkey = len(key)
                if lkey > mx: mx = lkey

            print('APPS')
            for key in self.apps:
                print('  '+key.ljust(mx+2,' '),self.apps[key]['help'])
            print()
    
    def openApp(self,appname):
        self.current_app = appname
        if 'onload' in self.apps[self.current_app]:
            self.apps[self.current_app]['onload']()
    
    def closeApp(self):
        self.current_app = None
    
    def command(self, route_str,app=None,help=''):
        '''command decorator'''
        def decorator(f):
            if app == None:
                self.commands[route_str] = {'f':f,'h':help}
            else:
                if not app in self.apps:
                    self.apps[app]={'commands':{}}
                self.apps[app]['commands'][route_str] = {'f':f,'h':help}
            return f
        return decorator

class App:
    def __init__(self,prompter,name,help=''):
        self.name = name
        self.commands = {}
        self.prompter = prompter
        self.help = help
        
        self.prompter.apps[name]={
            'commands':self.commands,
            'help':self.help,
        }
        
    def command(self, route_str,help=''):
        def decorator(f):
            self.prompter.apps[self.name]['commands'][route_str] = {'f':f,'h':help}
            return f
        return decorator

# ANSI Color Numbers
class colors:
    black='30'
    blue='34'
    cyan='36'
    darkgrey='90'
    green='32'
    orange='33'
    pink='95'
    purple='35'
    red='31'
    lightcyan='96'
    lightblue='94'
    lightgrey='37'
    lightred='91'
    lightgreen='92'
    yellow='93'