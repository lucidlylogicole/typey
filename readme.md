# Typey
A simple Python library for creating interactive command-based applications.  Somewhere between a CLI and TUI, although closer to a CLI.  When you want a little interaction and don't have complicated arguments to parse.

## Features
- simple decorator command routing
- continuous prompt until exiting
- multiple 'apps' can be set up inside a program with a separated routing namespace
- multiline input function
- error check wrapping commands with optionally displaying traceback or a simple error message

## Definitions
- **Prompter** - the main runner that parses and routes commands
- **Command** - the first word found before a space that is routed to a Python function.  Any text after the first word (and space) is passed to the function.
- **Argument** - in this case typey only has one that is passed to the commands and that is the string after the command.
- **App** - (optional) multiple apps can be loaded into the prompter with a separate namespace than other apps and the main commands.

## Install
- grab `typey.py` or copy the short codebase into your tool

----
## Quick Start

**typey_example.py**

    import typey
    prompt = typey.Prompter()
    
    @prompt.command('fib', help='fibonacci calculator')
    def fibonacci(n=1):
        n = int(n)
        if n == 0:
            print(0)
        else:
            t = 1
            for i in range(1,n+1):
                t = t*i
            print(t)
    
    prompt.start()

**Run**

    python3 typey_example.py

**Usage and Output**

    > fib
    1

    > fib 5
    120

    > fib s
    Traceback (most recent call last):
      File "../typey.py", line 71, in runInput
        funcD['f'](' '.join(cmds[1:]))
      File "typey_app_example.py", line 21, in fibonacci
        for i in range(1,int(n)+1):
    ValueError: invalid literal for int() with base 10: 's'

    > exit

## Examples
Additional examples of using typey in the `examples` directory of this repo include:
- basic use
- using an app
- simple todo app

----
## Documentation

### Built in Commands
- `exit` - exit the program or app (if in an app)
- `exit!` - exit the program even if in an app
- `clear` - clear the screen
- `help` - display a list of the available commands and apps

### Prompter
The prompter is the main object that runs the program and handles routing.

**Functions**
- **Prompter(name='')**
    - optional name that will show up in the prompt
- **Prompter.start(command=None)** - start the program
    - the program can be optionally started with a command
- **Prompter.exit()** - exit the program
- **Prompter.clear()** - clear the screen
- **Prompter.help()** - display a list of the available commands and apps
- **Prompter.input(text='')** - similar to the Python input() function, but uses the default prompt color
- **Prompter.minput(text='')** - a multiline input that will end after a blank line
    - note: the minput does not currently allow going back to the previous line to edit once return is pressed
- **Prompter.color(text,color_code=None)** - wrap text with the ANSI color code.
    - See the `colors` object for name based colors
    - set the color_code to None for no color
    - This may not work on some Windows terminals

**Settings**
- `Prompter.show_errors = True` - if True, show the Python traceback, otherwise just print 'ERROR' if false
- `Prompter.show_not_found = True` - if True, print 'not found' for any commands not found
- `Prompter.name = ''` - optionally set a name that will show up in the prompt for the program (`name> `).  Default is to just display ` > `
- `Prompter.prompt_color = colors.lightblue` - set the color of the prompt
    - this will not work in Windows, so the prompt_color should be `None`
- `Prompter.error_color = colors.lightred` - set the color of the error message
- `Prompter.mode` - a string holding the current mode and will also be displayed in the prompt.  The mode could be used for keeping state.

**Behind the Scenes Functions**  
Typically you wouldn't use these

- **Prompter.getPromptString()** - generates the formatted prompt string
- **Prompter.prompt(text=None)** - the main prompt execution function with optional text command to run.
- **Prompter.runInput(text)** - parses the text into a command and argument and routes the command and argument to the appropriate function
    - this can be called instead of `Prompter.prompt` to run commands without then sending the user back to the prompt.
- **Prompter.commands** - a dictionary holding the command routing
    - you could manually add things here instead of using the routes
- **Prompter.apps** - a dictionary holding the app routing
    - you could manually add things here instead of using the routes
- **Prompter.current_app = None** - if a subapp is loaded, the name of the app will be stored here, otherwise it will be none.

### Adding Commands to the prompter
A routing decorator similar to flask and other tools is utilized to map command strings to functions.

    prompt = typey.Prompter() # create the promtper first
    
    @prompt.command('hi', help='simple hello')
    def hello():
        print('hello')

    prompt.start()

Usage

    > hi
    hello

    > hi Joe
       ERROR

- notice the command name does not have to match the function name
- an error was generated in the second usage because the hello function does not accept any arguments.  To ignore an error like that setup the function with args to ignore like: `def hello(*args):`

### Commands with an argument
Typey will split the input text by a space and consider the first part the command and then pass the rest of the string to the function as a single argument.  Typey is simple and leaves the argument parsing to the function.

    @prompt.command('hi', help='simple hello')
    def hello(name=''):
        print(f'hello {name}')

Usage

    > hi
    hello 
    
    > hi Joe
    hello Joe
    
    > hi Joe Schmo
    hello Joe Schmo

### App
The app object is for creating sub apps inside the main Prompter program.  These apps will have a separated namespace for commands.

- **App(prompter, name, help='')**
    - **prompter** - the main program prompter (must be created before an app)
    - **name** - name of the app with *no spaces*
    - **help** - simple help string to display about the app



### Adding an app to the main program

    prompt = typey.Prompter() # create the main prompter program
    calc_app = typey.App(prompt,'calc') # create a new app
    
    @calc_app.command('sin', help='sine function')
    def sin(n):
        print(math.sin(float(n)))

    prompt.start()
    
Usage

    > calc
    calc> sin 20
    0.9129452507276277
    
    calc> sin a
        ERROR
    
    calc> exit
    > exit

- note that first the app must be started by entering the name of the app
- then the commands for that app can be used
- `exit` is also used to exit an app

### Colors
A simple ANSI color mapping object is included in typey, because why not.  These can be used with the `Prompter.color` function.

    prompt.color(colors.lightblue, 'Light Blue Text')

The colors only contain the color number, so if using them directly you would need to include some additional characters:

`"\033[{0}m".format(colors.lightblue)`

And then to stop using that color:

`"\033[00m"`

### Optionally pass sys args at start

**Example 1:** Start a typey program and run sys argv

    if len(sys.argv) > 1:
        prompt.start(' '.join(sys.argv[1:]))
    else:
        prompt.start()

**Example 2:** if sys arg passed just run the command, but don't start the program

    if len(sys.argv) > 1:
        prompt.runInput(' '.join(sys.argv[1:]))
    else:
        prompt.start()

----
## Future
The goal is to keep typey simple, but it would be nice to add a few things like command history and making the multiline input fully editable.

Probably typey will not come with any argument parsing functions, but there is a chance a basic one could be useful and may be added in the future.
